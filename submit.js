const c9request = require('./c9request');
const zipProblem = require('./zip-problem');
const fs = require('fs-extra');

let pid = null;
try {
 pid = fs.readFileSync('.config', 'utf8').trim();
} catch(e) {}

if(!pid && process.argv.length < 3) {
    console.log('Usage: tps-submit PROBLEM');
    process.exit();
}
if(pid && process.argv.length == 3 && process.argv[2] !== pid.trim()) {
    console.log('Vous vous trouvez dans le dossier du problème '+pid.trim()
  +' donc il n\'est pas possible de soumettre pour le problème '+process.argv[2]);
    process.exit();
}
if(pid && process.argv.length < 3) {
  console.log('Soumission du problème '+pid);
}
const problem = pid || process.argv[2];

const data = zipProblem();//fs.createReadStream('hello.c');//fs.readFileSync('hello.c');
//const data = fs.createReadStream('hello.c');//fs.readFileSync('hello.c');
//var o = fs.createWriteStream('example.zip');
//data.pipe(o);
data.then((buf) => {
    c9request.post('/c9api/submit', {problem: problem}, {files:{file:{data:buf, filename:'file.zip'}}});//.then(console.log);
});
