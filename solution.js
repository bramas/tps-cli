const c9request = require('./c9request');
const fs = require('fs-extra');
const readline = require('readline');

const options = {}
if(process.argv.length != 3) {
    console.log('usage: tps-solution ID_PROBLEM');
    process.exit(1);
}


const stdin = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

options.problem = process.argv[2];

options.handler = async function(data) {
    const problem = JSON.parse(data);

    if(!problem.solution) {
        console.log('Solution non disponible pour ce problème.');
        return;
    }

    solutionPath = process.env['HOME']+'/workspace/Problems/'+problem.folder+'/solution';
    simplifiedSolutionPath = '~/workspace/Problems/'+problem.folder+'/solution';
    const solutionPathExists = await fs.pathExists(solutionPath);
    if(solutionPathExists)
    {
        while(true) {
            const answer = await new Promise((accept, reject) => {
                stdin.question(`Le dossier "${simplifiedSolutionPath}" existe déjà. Le remplacer ? (y/n) `, accept);
            });
            if(answer === 'n' ||answer === 'N') {
                console.log("Opération annulée");
                process.exit();
            }
            else if(answer === 'y' ||answer === 'Y') {
                break;
            }
        }
    }
    await fs.mkdirs(solutionPath);

    for(var filename in problem.solution){
        let data = problem.solution[filename];
        await fs.writeFile(
            solutionPath+'/'+filename,
            new Buffer(data, 'base64')
        );
        console.log("Fichier copié :", simplifiedSolutionPath+'/'+filename);
    }

    stdin.close();

}

c9request.get('/c9api/solution', options);//.then(console.log);

