

if(process.argv.length < 3) {
    console.log('usage: tps-admin CMD OPTIONS');
    process.exit(1);
}



if(process.argv[2] === 'download-submissions') {
    require('./admin-download-submissions').apply(this, process.argv.slice(3));
} else {
    console.log('unknown command');
}


