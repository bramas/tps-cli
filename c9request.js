const http = require('http');
const crypto = require("crypto");
const axios = require('axios');
const request = require('request');
const fs = require('fs-extra');
const { Writable } = require('stream');

const TPS_SUBMISSION_URL= process.env.TPS_SUBMISSION_URL || 'https://api.tps.bramas.fr';



class CallbackStream extends Writable {
  constructor(callback) {
    super({});
    this.callback = callback;
  }
  _write(chunk, enc, next) {
    this.callback(chunk.toString());
    next();
  }
}


async function startServer() {

    const nonce = crypto.randomBytes(20).toString('hex');

    var server = http.createServer(function (req, res) {
      if(req.url === '/check?nonce='+nonce) {
          res.write('OK');
      } else {
          res.write('ERROR');
      }
      res.end(); //end the response
      server.close();
    });
    await server.listen(8082);
    return nonce;
}

async function getAccessKey() {
  const ak_location = process.env.HOME+'/.config/.tps-submission-access-key';

  if(!await fs.pathExists(ak_location)) {
    console.log('Veuillez enregistrer votre clé d\'accès avec la commande tps-save-access-key CLE');
    process.exit(0);
  }
  const key = await fs.readFile(ak_location, "utf8");
  return key.trim();
}

async function defaulParams() {
  if(process.env.C9_USER) {
    return {
        username: process.env.C9_USER,
        fullname: process.env.C9_FULLNAME,
        email: process.env.C9_EMAIL,
        project: process.env.C9_PROJECT,
        nonce: await startServer()
    };
  }
  const key = await getAccessKey();
  return {
    access_key: key
  };

}

async function c9request({endpoint, params, data, method}) {

    process.stdout.write('Création de la requête...');

    const defaultP = await defaulParams();

    const resultHandler = params.handler || process.stdout.write;

    params = Object.assign(params, defaultP);
    var options = {
        url:TPS_SUBMISSION_URL+endpoint,
        qs:params
    };

    var req = null;
    if(method=='post') {
        let files = {};
        let fields = {};
        for(var i in data){
            if(i == 'files') {
                for(var j in data[i]) {
                    files[data[i][j].filename] = data[i][j].data.toString('base64')
                    //req.form().append(j, data[i][j].data, {'filename': data[i][j].filename})
                }
                continue;
            }
            fields[i] = data[i];
        }
        options.body = {fields, files};
        options.json = true;
        req = request.post(options);
        //setInterval(() => console.log("Uploaded: "+req.req.connection._bytesDispatched), 250);
    } else
    if(method=='get') {
        req = request.get(options);
    } else {
        throw new Error('Unknown method <'+method+'>');
    }

    let receivedData = null;

    req.pipe(new CallbackStream((c) => {
        if(c.indexOf('<html>') === 0) {
            throw Error('Le serveur de soumission n\'est pas accessible');
        }
        if(receivedData !== null) {
            receivedData += c;
        } else {
            let startOfJson = c.indexOf('{');
            if(c.indexOf('[') !== -1 && c.indexOf('[') < startOfJson) {
                startOfJson = c.indexOf('[');
            }
            if(startOfJson !== -1) {
                receivedData = c.substring(startOfJson);
                c = c.substring(0, startOfJson);
            }
            process.stdout.write(c.replace(/\\u001b/mg, '\u001b'));
        }
    }));
    req.on('end', () => {
        resultHandler(receivedData);
        process.stdout.write('\n');
    });

    console.log('✔️');
}

module.exports = {
    async post(endpoint, params, data){
        return await c9request({
            endpoint,
            params,
            data,
            method:'post'
        });
    },
    async get(endpoint, params) {
        return await c9request({
            endpoint,
            params,
            method:'get'
        });
    },
}
