const c9request = require('./c9request');

if(process.argv.length < 3) {
    console.log('Usage: tps-submissions PROBLEM');
    process.exit();
}

const problem = process.argv[2];
c9request.get('/c9api/submissions', {problem: problem});//.then(console.log);