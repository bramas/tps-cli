const request = require('request');
const unzip = require('unzip-stream');
const fs = require('fs-extra');
const readline = require('readline');
const child_process = require('child_process');

const stdin = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const TPS_SUBMISSION_URL='https://api.tps.bramas.fr';

if(process.argv.length < 3) {
    console.log("Usage: tps-prepare PROBLEM");
    process.exit();
}

const problem = process.argv[2];
if(! /^[a-z0-9\-_]+$/i.test(problem)){
    console.log("Erreur: le nom du problème n'est pas acceptable");
    process.exit();
}
async function prepare(body) {
    problemPath = process.env['HOME']+'/workspace/Problems/'+body.folder;
    simplifiedProblemPath = '~/workspace/Problems/'+body.folder;
    const e = await fs.pathExists(problemPath);
    if(e)
    {
        while(true) {
            const answer = await new Promise((accept, reject) => {
                stdin.question(`Le dossier "${simplifiedProblemPath}" existe déjà. Le remplacer ? (y/n) `, accept);
            });
            if(answer === 'n' ||answer === 'N') {
                console.log("Opération annulée");
                process.exit();
            }
            else if(answer === 'y' ||answer === 'Y') {
                break;
            }
        }
    }
    await fs.mkdirs(problemPath);
    await fs.writeFile(problemPath+'/desc.md', body.description);
    await fs.writeFile(problemPath+'/.config', body.id);
    console.log("Description :           ", simplifiedProblemPath+'/desc.md');
    if(body.createMainFile) {
        await fs.ensureFile(problemPath+'/'+body.main);
        console.log("Fichier principal :     ", simplifiedProblemPath+'/'+body.main);
    }

    if(body.files) {
        for(var i in body.files){
            let f = body.files[i];
            await fs.writeFile(
                problemPath+'/'+f.filename,
                new Buffer(f.data, 'base64')
            );
            console.log("Fichier supplémentaire :", simplifiedProblemPath+'/'+f.filename);
        }
    }

    stdin.close();
    child_process.exec('c9 exec previewFile '+problemPath+'/desc.md');
}

async function getAccessKey() {
  const ak_location = process.env.HOME+'/.config/.tps-submission-access-key';

  if(await fs.pathExists(ak_location)) {
    const key = await fs.readFile(ak_location, "utf8");
    return key.trim();
  }
  return null;
}

async function send() {
    const access_key = await getAccessKey();

    request.get({ url:TPS_SUBMISSION_URL+'/api/prepare-problem/'+problem, qs:{access_key:access_key}},
    function(error, response, body) {
        if(error) {
            console.error(error);
            return;
        }
        body = JSON.parse(body);
        if(body.error) {
            console.error('Erreur:',body.error);
            return;
        }
        prepare(body).catch(console.error);

    });
}

send().then(() => { stdin.close(); }).catch(console.error);

   /* .pipe(unzip.Extract({ path: 'problems/'+problem }))
    .on('close', async function() {
		console.log('END');
		try {
			console.log('OK')
		} catch(e) {
			console.error(e);
		}
	}).on('error', function(e) {
		console.error(e);
	});*/
