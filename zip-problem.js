var archiver = require('archiver');

var stream = require('stream');  


module.exports = function() {
    return new Promise((accept, reject) => {
    // create a file to stream archive data to.
    /*var output = fs.createWriteStream(__dirname + '/example.zip');
    // listen for all archive data to be written
    output.on('close', function() {
      console.log(archive.pointer() + ' total bytes');
      console.log('archiver has been finalized and the output file descriptor has closed.');
    });
    
    */
    var archive = archiver('zip', {
        zlib: { level: 9 } // Sets the compression level.
    });
    
    
    // good practice to catch warnings (ie stat failures and other non-blocking errors)
    archive.on('warning', function(err) {
      if (err.code === 'ENOENT') {
          console.log(err);
      } else {
          // throw error
          throw err;
      }
    });
    
    // good practice to catch this error explicitly
    archive.on('error', function(err) {
      throw err;
    });
    
    // pipe archive data to the file
    var output = new stream.PassThrough();
    archive.pipe(output);
    
    archive.glob('Makefile');
    archive.glob('*.*', {ignore:'__main.c'});
    archive.glob('*/*.*', {ignore:'__main.c'});
    archive.finalize();
    var bufs = [];
    output.on('data', function(d){ bufs.push(d); });
    output.on('end', function(){
      var buf = Buffer.concat(bufs);
      accept(buf);
    });
});   
}
