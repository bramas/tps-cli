const c9request = require('./c9request');
const fs = require('fs-extra');
const readline = require('readline');
const request = require('request');
const extract = require('extract-zip');
const fstream = require('fstream');
const resolve = require('path').resolve;

module.exports = function(problem_id) {
    if(!problem_id) {
        console.log('missing options problem_id')
        process.exit(1);
    }


    const options = {}
    options.problem = problem_id;
    options.handler = async function(data) {
        //console.log(data);
        data = JSON.parse(data);



        if(data.length === 0) {
            console.log('no submissions');
            process.exit();
        }
        const problemPath = data[0].problem.folder;
        async function saveSubmissions(data) {
            for(let i in data) {
                const path = problemPath+'/'+data[i].user.fullname+'/'+data[i].submission_up_id;
                await fs.mkdirs(path);

                var writeStream = fstream.Writer(path);
                try {
                    const p = await new Promise(function(accept, reject) {
                        request(data[i].url)
                            .pipe(fs.createWriteStream(path+'/archive.zip'))
                            .on('close', function() {
                                extract(path+'/archive.zip', {dir: resolve(path)}, function (err) {
                                   if(err) {
                                       reject({err, path:path+'/archive.zip', url:data[i].url});
                                       return;
                                   }
                                   accept(path);
                                })
                            });
                    });
                    console.log('-----');
                    console.log('ok');
                    console.log(p);
                } catch(e) {
                    console.error('-----');
                    console.error('error');
                    console.error(e);
                }

            }
            return 'ok';
        }
        saveSubmissions(data).then(console.log).catch(console.error);

    }


    c9request.get('/c9api/download-submissions', options);//.then(console.log);

}