const c9request = require('./c9request');

if(process.argv.length < 3) {
    console.log('Usage: tps-request COMMAND');
    process.exit();
}

const command = process.argv[2];
const options = {}
if(process.argv.length > 3) {
    options.problem = process.argv[3];
}
c9request.get('/c9api/'+command, options);//.then(console.log);