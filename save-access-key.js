
const fs = require('fs-extra');

if(process.argv.length < 3) {
    console.log('Usage: tps-save-access-key KEY');
    process.exit();
}

const key = process.argv[2];

const ak_location = process.env.HOME+'/.config/.tps-submission-access-key';
fs.writeFile(ak_location, key).then(()=>{
  console.log("Clé enregistrée avec succès");
}).catch(console.error);
