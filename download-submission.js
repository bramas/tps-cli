const c9request = require('./c9request');

if(process.argv.length < 3) {
    console.log('Usage: tps-download-submission ID');
    process.exit();
}

const id = process.argv[2];
c9request.get('/c9api/submissions/'+id, {});